//Player variable
let player = document.querySelector(".player");
//Relative position so we can add to the position in order to move
player.style.position = "relative";

//Set initial position of the player at the center of the screen
player.style.left = (screen.width / 2) - 70 + "px"; //Half screen width, minus sprite width as it's origin is 0,0 at top left corner
player.style.top = (screen.height / 2) - 120 + "px";

window.addEventListener('keydown', input);
function input(event) 
{
    if (event.type === "keydown")
    {
        switch (event.keyCode)
        {
            case 37: //Debug
                //console.log("West");
                moveWest();
                //Flips the sprite
                flipWest();
                break;
            case 38: //Debug
                //console.log("North");
                flipNorth();
                moveNorth();
                break;
            case 39: //Debug
                //console.log("East");
                flipEast(); 
                moveEast();
                break;
            case 40: //Debug
                //console.log("South");
                flipSouth();
                moveSouth();
                break;
            default:
                break;      
        } 
    }
}

//                                 Flip functions
//=============================================================================================//
//               Flips the image accordingly to the sprites direction

//Flips the player depending on the direction
function flipWest()
{
    player.style.transform = 'rotate(-90deg)';
}
//Flips the player depending on the direction
function flipEast()
{
    player.style.transform = 'rotate(90deg)';
}
//Flips the player depending on the direction
function flipNorth()
{
    player.style.transform = 'rotate(0deg)';
}
//Flips the player depending on the direction
function flipSouth()
{
    player.style.transform = 'rotate(180deg)';
}

//                                 Move functions
//=============================================================================================//
//                    Moves the "player" accordingly to the input

//Move functions
function moveWest()
{
    player.style.left = parseInt(player.style.left) - 10 + "px"
}
function moveNorth()
{
    player.style.top = parseInt(player.style.top) - 10 + "px"
}
function moveEast()
{
    player.style.left = parseInt(player.style.left) + 10 + "px"
}
function moveSouth()
{
    player.style.top = parseInt(player.style.top) + 10 + "px"
}