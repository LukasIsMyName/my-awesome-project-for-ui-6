console.log("Js works");

//Player variables
let sizeX = 70;
let sizeY = 120;
let speed = 5;

//Canvas
const canvas = document.getElementById("gameCanvas");
const context = canvas.getContext("2d");

//Player position
let posX = (canvas.width / 2 ) - sizeX;
let posY = (canvas.height / 2) - sizeY; 

//Image variables
let shipImage = new Image();
let backgroundImage = new Image();


function draw()
{
    shipImage.src = "assets/images/ship.png";
    shipImage.onload = function()
    {
        context.drawImage(shipImage, posX, posY, sizeX, sizeY);
        console.log("Image drawn succesfully");
    }
    console.log("function called");
}

//Call the function to draw
draw();

document.addEventListener("keydown", input);
document.addEventListener("keyup", input);
function input(event) 
{
    if (event.type === "keydown") //When pressed
    {
        switch (event.keyCode)
        {
            
            case 37: case 65: //Debug
                console.log("West");
                moveWest();
                break;
            case 38: case 87: //Debug
                console.log("North");
                moveNorth();
                break;
            case 39: case 68: //Debug
                console.log("East");
                moveEast();
                break;
            case 40: case 83: //Debug
                console.log("South");
                moveSouth();
                break;
            default:
                break;      
        } 
    }
    if (event.type === "keyup") //When released
    {
        switch (event.keyCode)
        {
            case 32:
                playAudio(); //Debug
                console.log("Audio playing");
        }
    }

}

//                                 Move functions
//=============================================================================================//
//                    Moves the "player" accordingly to the input

//Move functions
function moveWest()
{
    posX -= speed;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(shipImage, posX, posY, sizeX, sizeY);
}
function moveNorth()
{
    posY -= speed;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(shipImage, posX, posY, sizeX, sizeY);
}
function moveEast()
{
    posX += speed;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(shipImage, posX, posY, sizeX, sizeY);
}
function moveSouth()
{
    posY += speed;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(shipImage, posX, posY, sizeX, sizeY);
}

//                                 Play Audio Function
//=============================================================================================//
//                            Plays audio upon function call

function playAudio()
{
    let audio = new Audio('assets/media/Ok_lets_go.wav');
    audio.play();
    audio.volume = 0.05;
}